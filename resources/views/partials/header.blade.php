<header id="header" class="fixed-top ">
  <div class="container d-flex align-items-center">

    <h1 class="logo mr-auto"><a href="{{ route('index')}}">Yuvaleads</a></h1>

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <!--<li class="active"><a href="index.html">Home</a></li>-->
        <li class="active"><a href="/">Home</a></li>
        <li><a href="/#about">About</a></li>
        <li><a href="/#services">Our Activities</a></li>
        <li><a href="{{ route('ourteam') }}">Team</a></li>
        <li><a href="/#contact">Contact</a></li>

      </ul>
    </nav><!-- .nav-menu -->

  </div>
</header><!-- End Header -->
