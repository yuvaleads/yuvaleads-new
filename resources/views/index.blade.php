@extends('layouts.app')
@section('content')
  <section id="hero">
    <div class="hero-container">
      <h3>Welcome to <strong>Yuvaleads</strong></h3>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title">
          <h2>About Us</h2>
        </div>

        <div class="row content">
          <div class ="col-lg-1">
          </div>
          <div class="col-lg-10">
            <h5>
              YuvaLeads is an organisation created and directed by a group of enthusiasts to create a strong, vibrant, intelligent student community locally, and nationally. YuvaLeads is envisioned as a platform to provide opportunities to the students and to enable them to explore their strengths and know themselves and their interests, ambitions and give them a clearer picture of the real side of the industry and leadership. We propose to take a two-way approach to create this strong community of the students. Our objective is to take the student interns and volunteers through the various stages of leadership and soft skill development to prepare them for a better career ahead.
            </h5>
          </div>
          <div class ="col-lg-1">
            <!--<div class="col-sm-12">
              <h4><a class="nav-link" href="{{ route('ourteam') }}">Meet Our Team &rarr;</a></h4>
            </div>-->
          </div>
        </div>
        <hr>
        <br>
        <div class="row content">
          <div class ="col-lg-6">
            <div>
                <h4>Our Mission</h4>
                <ol type = 1>
                  <li>Building a strong Youth community, which is focused towards making positive contributions to the growth of the individual, the community and the nation. </li>
                  <li>Creating a more thoughtful and deeply engaged class of youth who would be willing to be the change agent and be far more empathetic towards the social and political climate of our country.</li>
                  <li>Influence the government to involve more youth and create youth centric policies for the economic development of our country.</li>
                  <li>Create a talented pool of professionals who have the initiative taking ability to guide the country into creating and leading corporate giants.</li>
                </ol>
            </div>
          </div>
          <div class="col-lg-6">
            <div>
                <h4>Our Plan of Action</h4>
                <ol>
                  <li>Training the Youth to take leadership roles at Social, Political and Corporate levels for creating a New Bharat.</li>
                  <li>Engaging the student community in large to help them understand the gaps in their education.</li>
                  <li>Creating skill programmes and workshops to fill in the discovered gaps in the system.</li>
                  <li>Provide youngsters with a platform to discover their hidden potential and help them understand if that can be taken up for profession.</li>
                  <li>Create and conduct workshops / trips and programmes around the culture and history of our country to help them get acquainted with the past.</li>
                  <li>Provide a platform to young startups to explore their idea and pursue it fearlessly (yuvastarts).</li>
                  <li>Enable the youth from tier 2 and tier 3 cities to implement their ideas into money earning opportunities.</li>
                </ol>
                <!-- END SINGLE PROGRESS BAR -->
            </div>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h1>Our Activities</h1>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Leadership Talks And Retreats</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Courses & Workshops</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Mentoring Opportunities</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Volunteering Opportunities</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Internship Opportunities</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Student Exchange Programmes</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Model Parliament</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Contributors Conferences</h4>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-briefcase'></i></div>
              <h4 class="title">Culture Clubs</h4>
            </div>
          </div>
        </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="text-center">
          <h3>Volunteer With Us</h3>
          <p>To Volunteer with us, fill up the form by clicking on the link</p>
          <a class="cta-btn" href="#">Call To Action</a>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <h2>Our Partners</h2>
        </div>
        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <h3>Coming Soon</h3>
          </div>
        </div>
      </div>
    </section><!-- End Portfolio Section -->

    @include('partials.contact')
  </main><!-- End #main -->
@endsection
