@extends('layouts.app')
@section('content')
<section id="hero">
	<div class="hero-container">
		<h3>Welcome to <strong>Yuvaleads</strong></h3>
	</div>
</section><!-- End Hero -->

<main id="main">
<section id="team" class="team">
  <div class="container">
    <div class="section-title">
      <h2>Team</h2>
      <h3>Our <span>Team</span></h3>
    </div>
    <!--START INTERNS-->
    <div class="col-sm-12">
      <div class="section-title">
        <h4>Interns</h4>
        <!-- START ROW -->
        <div class="row">
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/13.JPG" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/disha-shah-9824881a4/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>

              </div>
              <div class="member-info">
                <h4>Disha Shah</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/18.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/smiti-patro-24bb74170" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Smiti Patro</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/16.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Mayuresh Veeramallu</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/17.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/harshsoni22" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Harsh Soni</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>
					<div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/9.jpg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/snehal-garde-8b864a136/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Snehal Garde</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/10.png" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/pracheta-khot-a2344618b/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Pracheta Khot</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/11.jpg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/nitish-chaulkar-905a96141/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Nitish Chaulkar</h4>
              </div>
            </div>
          </div>
        </div>
        <!--END ROW-->

      </div>
    </div>
    <!--END INTERNS-->
    <!--START CORE TEAM-->
    <div class="col-sm-12">
      <div class="section-title">
        <h4>Core team</h4>
        <!-- START ROW -->
        <div class="row">
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/8.jpg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/simran-bajpei-b8761966" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Simran Bajpei</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/12.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/mayuresh-pawaskar-7a4aa61a4" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Mayuresh Pawaskar</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/15.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/manish-rai-8194531a8" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Manish Rai</h4>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/19.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/karishma-singh-2a3ba31ab" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Karishma Singh</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>
        </div>
        <!--END ROW-->
      </div>
    </div>
    <!--END CORE TEAM-->
    <!--START CO-FOUNDER-->
    <div class="col-sm-12">
      <div class="section-title">
        <h4>Co-Founder</h4>
        <!-- START ROW -->
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/5.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/kiran-murkar-7218ba14/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Kiran Murkar</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/6.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/surendra-mali-465779a4/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Surendra Mali</h4>
                <!--<span>Product Manager</span>-->
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 ">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/7.jpeg" data-holder-rendered="true">
                <div class="social">
                  <!--<a href=""><i class="icofont-twitter"></i></a>
                    <a href=""><i class="icofont-facebook"></i></a>
                    <a href=""><i class="icofont-instagram"></i></a>-->
                  <a href="https://www.linkedin.com/in/shubh-jangam/" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Shubh Jangam</h4>
                <!--<span>Product Manager</span>-->
              </div>
            </div>
          </div>
        </div>
        <!--END ROW-->
      </div>
    </div>
    <!--END CO_FOUNDER-->
    <!--START FOUNDER-->
    <div class="col-sm-12">
      <div class="section-title">
        <h4>Founder</h4>
        <!-- START ROW -->
        <div class="row">
          <div class ="col-lg-4">
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="member">
              <div class="member-img">
                <img class="rounded-circle" alt="" width="200" height="200" src="assets/img/team/4.jpeg" data-holder-rendered="true">
                <div class="social">
                  <a href="https://www.linkedin.com/in/harshsmodi" target="_blank"><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Harsh Modi</h4>
                <!--<span>Chief Executive Officer</span>-->
              </div>
            </div>
          </div>
          <div class ="col-lg-4">
          </div>
        </div>
        <!--END ROW-->
      </div>
    </div>
    <!--END FOUNDER-->
  </div>
</section>
</main>
<!-- End Team Section -->
@endsection
