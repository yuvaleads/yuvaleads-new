<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
//use UxWeb\SweetAlert\SweetAlert;
class FrontEndController extends Controller
{
  public function index()
  {
    return view('index');
  }
  public function send_volunteer_email(Request $request)
  {
    Mail::send('emails', ['name' => $request->name, 'email'=>$request->email,'phone_number'=>$request->number,'query' => $request->message], function ($message) use ($request)
    {
      $message->subject("Volunteer with us");
      $message->to('hello@yuvaleads.org');
      $message->from('yuvaleads@gmail.com');
    });
    SweetAlert::success('message','We will get back to you within 24 hours!');
    return redirect()->route('index');
  }
  public function send_contact_email(Request $request)
  {
    Mail::send('emails', ['name' => $request->name, 'email'=>$request->email,'phone_number'=>$request->number,'query' => $request->message], function ($message) use ($request)
    {
      $message->subject("Contact Us");
      $message->to('hello@yuvaleads.org');
      $message->from('yuvaleads@gmail.com');
    });
    SweetAlert::success('message','We will get back to you within 24 hours!');
    return redirect()->route('index')->with('message','We will get back to you within 24 hours!');
  }

  public function prepintern()
  {
    return view('prepintern');
  }

  public function ourteam()
  {
    return view('ourteam');
  }

  


}
