<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses'=>'FrontEndController@index','as'=>'index']);
Route::post('/send-contact-email',['uses'=>'FrontEndController@send_contact_email','as'=>'send_contact_email']);
Route::get('/our-team', 'FrontEndController@ourteam')->name('ourteam');
